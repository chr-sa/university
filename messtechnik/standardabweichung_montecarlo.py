import numpy as np
import matplotlib.pyplot as plt

def estimate_normal(n_samples: int):
    # Draw samples from a normal distribution.
    samples = np.random.normal(loc=10, scale=1, size=n_samples)

    # Estimate mean and standard deviation from the samples.
    mean = (1 / n_samples) * np.sum(samples)
    std = np.sqrt((1 / (n_samples - 1)) * np.sum((samples - mean)**2))

    return mean, std

def monte_carlo(n_experiments: int):
    N = [2, 3, 4, 5]
    data = dict()
    fig, (ax1, ax2) = plt.subplots(1, 2)

    for n in N:
        data[n] = {"mean": list(), "std": list()}
        for _ in range(n_experiments):
            mean, std = estimate_normal(n)
            data[n]["mean"].append(mean)
            data[n]["std"].append(std)
        mean_hist, mean_bin_edges = np.histogram(data[n]["mean"], bins=20)
        mean_centers = bins2centers(mean_bin_edges)
        ax1.plot(mean_centers, mean_hist)

        std_hist, std_bin_edges = np.histogram(data[n]["std"], bins=20)
        std_centers = bins2centers(std_bin_edges)
        ax2.plot(std_centers, std_hist)

    plt.show()

def bins2centers(bin_edges: np.ndarray):
    # Calculate the center of the bins.
    centers = list()
    for i in range(len(bin_edges) - 1):
        center = (bin_edges[i] + bin_edges[i + 1]) / 2
        centers.append(center)

    return np.array(centers)

if __name__ == "__main__":
    monte_carlo(10000)
